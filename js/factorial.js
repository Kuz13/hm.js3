let number = prompt("Введите ваше число");
while (isNaN(number) || number < 1) {
    number = prompt("Введите ваше число");
}
number = Number(number);

function factorial(n) {
    number = n;
  return (n != 1) ? n * factorial(n - 1) : 1;
}
console.log("Факториал числа " + number + " - " + factorial(number));